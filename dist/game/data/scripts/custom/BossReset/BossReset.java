/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package custom.BossReset;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.l2jmobius.gameserver.instancemanager.DBSpawnManager;
import com.l2jmobius.gameserver.model.L2Object;
import com.l2jmobius.gameserver.model.L2World;
import com.l2jmobius.gameserver.model.actor.L2Npc;
import com.l2jmobius.gameserver.model.actor.instance.L2GrandBossInstance;
import com.l2jmobius.gameserver.model.actor.instance.L2PcInstance;
import com.l2jmobius.gameserver.model.quest.Quest;
import com.l2jmobius.gameserver.util.Util;

/**
 * @author Administrator
 */
public class BossReset extends Quest
{
	private static int dist = 300;// Boss 最远能拖走1600距离
	private static int[] _grandbossId =
	{
		29001
	};// 世界级的boss必须加载这里,小boss不需要加
	private final List<L2Npc> _npcs = new ArrayList<>();
	private final Set<Integer> _grandboss = new HashSet<>();
	
	private void intList()
	{
		for (L2Npc boss : DBSpawnManager.getInstance().getNpcs().values())
		{
			_npcs.add(boss);
		}
		
		for (int element : _grandbossId)
		{
			_grandboss.add(element);
		}
	}
	
	@Override
	public final String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		if (event.equals("check"))
		{
			for (L2Npc boss : _npcs)
			{
				checkDist(boss);
			}
			
			for (L2Object obj : L2World.getInstance().getVisibleObjects())
			{
				if (!(obj instanceof L2GrandBossInstance))
				{
					continue;
				}
				
				L2Npc Boss = (L2Npc) obj;
				if (_grandboss.contains(Boss.getId()))
				{
					checkDist(Boss);
				}
			}
		}
		return null;
	}
	
	/**
	 * @param Boss
	 */
	private void checkDist(L2Npc Boss)
	{
		if (Boss == null)
		{
			return;
		}
		
		int lastx, lasty, lastz, x, y, z;
		lastx = Boss.getSpawn().getX();
		lasty = Boss.getSpawn().getY();
		lastz = Boss.getSpawn().getZ();
		x = Boss.getX();
		y = Boss.getY();
		z = Boss.getZ();
		if (Util.calculateDistance(x, y, z, lastx, lasty, lastz, true, isActive()) > dist)
		{
			Boss.teleToLocation(lastx, lasty, lastz);
			Boss.setCurrentHpMp(Boss.getCurrentHp() + Boss.getMaxHp(), Boss.getMaxMp());
		}
	}
	
	/**
	 * @param questId
	 * @param name
	 * @param descr
	 */
	public BossReset(int questId, String name, String descr)
	{
		super(questId);
		intList();
		startQuestTimer("check", 20000, null, null, true);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new BossReset(-1, "BossReset", "custom");
	}
}
