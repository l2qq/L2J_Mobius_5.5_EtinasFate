/*
 * This file is part of the L2J Mobius project.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.l2jmobius.Config;
import com.l2jmobius.gameserver.data.xml.impl.AdminData;
import com.l2jmobius.gameserver.enums.ChatType;
import com.l2jmobius.gameserver.handler.IAdminCommandHandler;
import com.l2jmobius.gameserver.model.L2Object;
import com.l2jmobius.gameserver.model.L2World;
import com.l2jmobius.gameserver.model.actor.instance.L2PcInstance;
import com.l2jmobius.gameserver.model.entity.Hero;
import com.l2jmobius.gameserver.model.olympiad.Olympiad;
import com.l2jmobius.gameserver.network.SystemMessageId;
import com.l2jmobius.gameserver.network.serverpackets.CreatureSay;
import com.l2jmobius.gameserver.network.serverpackets.ExWorldChatCnt;
import com.l2jmobius.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jmobius.gameserver.util.BuilderUtil;
import com.l2jmobius.gameserver.util.Util;

/**
 * This class handles following admin commands: - admin|admin1/admin2/admin3/admin4/admin5 = slots for the 5 starting admin menus - gmliston/gmlistoff = includes/excludes active character from /gmlist results - silence = toggles private messages acceptance mode - diet = toggles weight penalty mode -
 * tradeoff = toggles trade acceptance mode - reload = reloads specified component from multisell|skill|npc|htm|item - set/set_menu/set_mod = alters specified server setting - saveolymp = saves olympiad state manually - manualhero = cycles olympiad and calculate new heroes.
 * @version $Revision: 1.3.2.1.2.4 $ $Date: 2007/07/28 10:06:06 $
 */
public class AdminAdmin implements IAdminCommandHandler
{
	private static final Logger LOGGER = Logger.getLogger(AdminAdmin.class.getName());
	
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_admin",
		"admin_admin1",
		"admin_admin2",
		"admin_admin3",
		"admin_admin4",
		"admin_admin5",
		"admin_admin6",
		"admin_admin7",
		"admin_gmliston",
		"admin_gmlistoff",
		"admin_silence",
		"admin_diet",
		"admin_tradeoff",
		"admin_set",
		"admin_set_mod",
		"admin_saveolymp",
		"admin_sethero",
		"admin_settruehero",
		"admin_givehero",
		"admin_endolympiad",
		"admin_setconfig",
		"admin_config_server",
		"admin_gmon",
		"admin_worldchat",
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		if (command.startsWith("admin_admin"))
		{
			showMainPage(activeChar, command);
		}
		else if (command.equals("admin_config_server"))
		{
			showConfigPage(activeChar);
		}
		else if (command.startsWith("admin_gmliston"))
		{
			AdminData.getInstance().showGm(activeChar);
			BuilderUtil.sendSysMessage(activeChar, "登记到GM列表.");
			AdminHtml.showAdminHtml(activeChar, "gm_menu.htm");
		}
		else if (command.startsWith("admin_gmlistoff"))
		{
			AdminData.getInstance().hideGm(activeChar);
			BuilderUtil.sendSysMessage(activeChar, "从GM列表中移除.");
			AdminHtml.showAdminHtml(activeChar, "gm_menu.htm");
		}
		else if (command.startsWith("admin_silence"))
		{
			if (activeChar.isSilenceMode()) // already in message refusal mode
			{
				activeChar.setSilenceMode(false);
				activeChar.sendPacket(SystemMessageId.MESSAGE_ACCEPTANCE_MODE);
			}
			else
			{
				activeChar.setSilenceMode(true);
				activeChar.sendPacket(SystemMessageId.MESSAGE_REFUSAL_MODE);
			}
			AdminHtml.showAdminHtml(activeChar, "gm_menu.htm");
		}
		else if (command.startsWith("admin_saveolymp"))
		{
			Olympiad.getInstance().saveOlympiadStatus();
			BuilderUtil.sendSysMessage(activeChar, "奥赛系统已存储.");
		}
		else if (command.startsWith("admin_endolympiad"))
		{
			try
			{
				Olympiad.getInstance().manualSelectHeroes();
			}
			catch (Exception e)
			{
				LOGGER.warning("An error occured while ending olympiad: " + e);
			}
			BuilderUtil.sendSysMessage(activeChar, "英雄已涌现.");
		}
		else if (command.startsWith("admin_sethero"))
		{
			if (activeChar.getTarget() == null)
			{
				activeChar.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			
			final L2PcInstance target = activeChar.getTarget().isPlayer() ? activeChar.getTarget().getActingPlayer() : activeChar;
			target.setHero(!target.isHero());
			target.broadcastUserInfo();
		}
		else if (command.startsWith("admin_settruehero"))
		{
			if (activeChar.getTarget() == null)
			{
				activeChar.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			
			final L2PcInstance target = activeChar.getTarget().isPlayer() ? activeChar.getTarget().getActingPlayer() : activeChar;
			target.setTrueHero(!target.isTrueHero());
			target.broadcastUserInfo();
		}
		else if (command.startsWith("admin_givehero"))
		{
			if (activeChar.getTarget() == null)
			{
				activeChar.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			
			final L2PcInstance target = activeChar.getTarget().isPlayer() ? activeChar.getTarget().getActingPlayer() : activeChar;
			if (Hero.getInstance().isHero(target.getObjectId()))
			{
				BuilderUtil.sendSysMessage(activeChar, "该玩家已是英雄.");
				return false;
			}
			
			if (!Hero.getInstance().isUnclaimedHero(target.getObjectId()))
			{
				BuilderUtil.sendSysMessage(activeChar, "该玩家还不是英雄.");
				return false;
			}
			Hero.getInstance().claimHero(target);
		}
		else if (command.startsWith("admin_diet"))
		{
			try
			{
				final StringTokenizer st = new StringTokenizer(command);
				st.nextToken();
				if (st.nextToken().equalsIgnoreCase("on"))
				{
					activeChar.setDietMode(true);
					BuilderUtil.sendSysMessage(activeChar, "Diet mode on.");
				}
				else if (st.nextToken().equalsIgnoreCase("off"))
				{
					activeChar.setDietMode(false);
					BuilderUtil.sendSysMessage(activeChar, "Diet mode off.");
				}
			}
			catch (Exception ex)
			{
				if (activeChar.getDietMode())
				{
					activeChar.setDietMode(false);
					BuilderUtil.sendSysMessage(activeChar, "Diet mode off.");
				}
				else
				{
					activeChar.setDietMode(true);
					BuilderUtil.sendSysMessage(activeChar, "Diet mode on.");
				}
			}
			finally
			{
				activeChar.refreshOverloaded(true);
			}
			AdminHtml.showAdminHtml(activeChar, "gm_menu.htm");
		}
		else if (command.startsWith("admin_tradeoff"))
		{
			try
			{
				final String mode = command.substring(15);
				if (mode.equalsIgnoreCase("on"))
				{
					activeChar.setTradeRefusal(true);
					BuilderUtil.sendSysMessage(activeChar, "交易开启.");
				}
				else if (mode.equalsIgnoreCase("off"))
				{
					activeChar.setTradeRefusal(false);
					BuilderUtil.sendSysMessage(activeChar, "交易关闭.");
				}
			}
			catch (Exception ex)
			{
				if (activeChar.getTradeRefusal())
				{
					activeChar.setTradeRefusal(false);
					BuilderUtil.sendSysMessage(activeChar, "交易关闭.");
				}
				else
				{
					activeChar.setTradeRefusal(true);
					BuilderUtil.sendSysMessage(activeChar, "交易开启.");
				}
			}
			AdminHtml.showAdminHtml(activeChar, "gm_menu.htm");
		}
		else if (command.startsWith("admin_setconfig"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			st.nextToken();
			try
			{
				final String pName = st.nextToken();
				final String pValue = st.nextToken();
				if (Float.valueOf(pValue) == null)
				{
					BuilderUtil.sendSysMessage(activeChar, "参数无效!");
					return false;
				}
				switch (pName)
				{
					case "RateXp":
					{
						Config.RATE_XP = Float.valueOf(pValue);
						break;
					}
					case "RateSp":
					{
						Config.RATE_SP = Float.valueOf(pValue);
						break;
					}
					case "RateDropSpoil":
					{
						Config.RATE_SPOIL_DROP_CHANCE_MULTIPLIER = Float.valueOf(pValue);
						break;
					}
					case "EnchantChanceElementStone":
					{
						Config.ENCHANT_CHANCE_ELEMENT_STONE = Float.valueOf(pValue);
						break;
					}
					case "EnchantChanceElementCrystal":
					{
						Config.ENCHANT_CHANCE_ELEMENT_CRYSTAL = Float.valueOf(pValue);
						break;
					}
					case "EnchantChanceElementJewel":
					{
						Config.ENCHANT_CHANCE_ELEMENT_JEWEL = Float.valueOf(pValue);
						break;
					}
					case "EnchantChanceElementEnergy":
					{
						Config.ENCHANT_CHANCE_ELEMENT_ENERGY = Float.valueOf(pValue);
						break;
					}
				}
				BuilderUtil.sendSysMessage(activeChar, "配置参数 " + pName + " 调至 " + pValue);
			}
			catch (Exception e)
			{
				BuilderUtil.sendSysMessage(activeChar, "使用命令: //setconfig <parameter> <value>");
			}
			finally
			{
				showConfigPage(activeChar);
			}
		}
		else if (command.startsWith("admin_worldchat"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			st.nextToken(); // admin_worldchat
			switch (st.hasMoreTokens() ? st.nextToken() : "")
			{
				case "shout":
				{
					final StringBuilder sb = new StringBuilder();
					while (st.hasMoreTokens())
					{
						sb.append(st.nextToken());
						sb.append(" ");
					}
					
					final CreatureSay cs = new CreatureSay(activeChar, ChatType.WORLD, sb.toString());
					L2World.getInstance().getPlayers().stream().filter(activeChar::isNotBlocked).forEach(cs::sendTo);
					break;
				}
				case "see":
				{
					final L2Object target = activeChar.getTarget();
					if ((target == null) || !target.isPlayer())
					{
						activeChar.sendPacket(SystemMessageId.THAT_IS_AN_INCORRECT_TARGET);
						break;
					}
					final L2PcInstance targetPlayer = target.getActingPlayer();
					if (targetPlayer.getLevel() < Config.WORLD_CHAT_MIN_LEVEL)
					{
						BuilderUtil.sendSysMessage(activeChar, "您的目标等级低于预设最低等级: " + Config.WORLD_CHAT_MIN_LEVEL);
						break;
					}
					BuilderUtil.sendSysMessage(activeChar, "玩家「" + targetPlayer.getName() + "」: 使用世界聊天 " + targetPlayer.getWorldChatUsed() + " 时间超时( " + targetPlayer.getWorldChatPoints() + ") 点.");
					break;
				}
				case "set":
				{
					final L2Object target = activeChar.getTarget();
					if ((target == null) || !target.isPlayer())
					{
						activeChar.sendPacket(SystemMessageId.THAT_IS_AN_INCORRECT_TARGET);
						break;
					}
					
					final L2PcInstance targetPlayer = target.getActingPlayer();
					if (targetPlayer.getLevel() < Config.WORLD_CHAT_MIN_LEVEL)
					{
						BuilderUtil.sendSysMessage(activeChar, "您的目标等级低于预设最低等级: " + Config.WORLD_CHAT_MIN_LEVEL);
						break;
					}
					
					if (!st.hasMoreTokens())
					{
						BuilderUtil.sendSysMessage(activeChar, "语法错误, 请使用: //worldchat set <times used>");
						break;
					}
					
					final String valueToken = st.nextToken();
					if (!Util.isDigit(valueToken))
					{
						BuilderUtil.sendSysMessage(activeChar, "语法错误, 请使用:: //worldchat set <times used>");
						break;
					}
					
					BuilderUtil.sendSysMessage(activeChar, "玩家「" + targetPlayer.getName() + "」: 世界聊天时数 " + targetPlayer.getWorldChatPoints() + " 变更为 " + valueToken);
					targetPlayer.setWorldChatUsed(Integer.parseInt(valueToken));
					if (Config.ENABLE_WORLD_CHAT)
					{
						targetPlayer.sendPacket(new ExWorldChatCnt(targetPlayer));
					}
					break;
				}
				default:
				{
					BuilderUtil.sendSysMessage(activeChar, "可使用的命令如下:");
					BuilderUtil.sendSysMessage(activeChar, " - 发送信息: //worldchat shout <text>");
					BuilderUtil.sendSysMessage(activeChar, " - 查看目标世界聊天点数: //worldchat see");
					BuilderUtil.sendSysMessage(activeChar, " - 变更目标世界聊天点数: //worldchat set <points>");
					break;
				}
			}
		}
		else if (command.startsWith("admin_gmon"))
		{
			// TODO why is this empty?
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
	
	private void showMainPage(L2PcInstance activeChar, String command)
	{
		int mode = 0;
		String filename = null;
		try
		{
			mode = Integer.parseInt(command.substring(11));
		}
		catch (Exception e)
		{
		}
		switch (mode)
		{
			case 1:
			{
				filename = "main";
				break;
			}
			case 2:
			{
				filename = "game";
				break;
			}
			case 3:
			{
				filename = "effects";
				break;
			}
			case 4:
			{
				filename = "server";
				break;
			}
			case 5:
			{
				filename = "mods";
				break;
			}
			case 6:
			{
				filename = "char";
				break;
			}
			case 7:
			{
				filename = "gm";
				break;
			}
			default:
			{
				filename = "main";
				break;
			}
		}
		AdminHtml.showAdminHtml(activeChar, filename + "_menu.htm");
	}
	
	private void showConfigPage(L2PcInstance activeChar)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage();
		final StringBuilder replyMSG = new StringBuilder("<html><title>天堂II配置信息</title><body>");
		replyMSG.append("<center><table width=270><tr><td width=60><button value=\"首页\" action=\"bypass -h admin_admin\" width=60 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td><td width=150>服务器设定界面</td><td width=60><button value=\"返回\" action=\"bypass -h admin_admin4\" width=60 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr></table></center><br>");
		replyMSG.append("<center><table width=260><tr><td width=140></td><td width=40></td><td width=40></td></tr>");
		replyMSG.append("<tr><td><font color=\"00AA00\">掉落:</font></td><td></td><td></td></tr>");
		replyMSG.append("<tr><td><font color=\"LEVEL\">经验倍率</font> = " + Config.RATE_XP + "</td><td><edit var=\"param1\" width=40 height=15></td><td><button value=\"设定\" action=\"bypass -h admin_setconfig RateXp $param1\" width=40 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
		replyMSG.append("<tr><td><font color=\"LEVEL\">技能点数</font> = " + Config.RATE_SP + "</td><td><edit var=\"param2\" width=40 height=15></td><td><button value=\"设定\" action=\"bypass -h admin_setconfig RateSp $param2\" width=40 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
		replyMSG.append("<tr><td><font color=\"LEVEL\">回收倍率</font> = " + Config.RATE_SPOIL_DROP_CHANCE_MULTIPLIER + "</td><td><edit var=\"param4\" width=40 height=15></td><td><button value=\"设定\" action=\"bypass -h admin_setconfig RateDropSpoil $param4\" width=40 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
		replyMSG.append("<tr><td width=140></td><td width=40></td><td width=40></td></tr>");
		replyMSG.append("<tr><td><font color=\"00AA00\">强化几率:</font></td><td></td><td></td></tr>");
		replyMSG.append("<tr><td><font color=\"LEVEL\">元素强化: 原石</font> = " + Config.ENCHANT_CHANCE_ELEMENT_STONE + "</td><td><edit var=\"param8\" width=40 height=15></td><td><button value=\"设定\" action=\"bypass -h admin_setconfig EnchantChanceElementStone $param8\" width=40 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
		replyMSG.append("<tr><td><font color=\"LEVEL\">元素强化: 水晶</font> = " + Config.ENCHANT_CHANCE_ELEMENT_CRYSTAL + "</td><td><edit var=\"param9\" width=40 height=15></td><td><button value=\"设定\" action=\"bypass -h admin_setconfig EnchantChanceElementCrystal $param9\" width=40 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
		replyMSG.append("<tr><td><font color=\"LEVEL\">元素强化: 宝石</font> = " + Config.ENCHANT_CHANCE_ELEMENT_JEWEL + "</td><td><edit var=\"param10\" width=40 height=15></td><td><button value=\"设定\" action=\"bypass -h admin_setconfig EnchantChanceElementJewel $param10\" width=40 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
		replyMSG.append("<tr><td><font color=\"LEVEL\">元素强化: 能量</font> = " + Config.ENCHANT_CHANCE_ELEMENT_ENERGY + "</td><td><edit var=\"param11\" width=40 height=15></td><td><button value=\"设定\" action=\"bypass -h admin_setconfig EnchantChanceElementEnergy $param11\" width=40 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
		
		replyMSG.append("</table></body></html>");
		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}
}
